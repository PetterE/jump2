'''
Created on 16 jun 2013

@author: Petter
'''
import pyglet
from game import screen_handler

# Instancing my screen handler class
screen_handler = screen_handler.ScreenHandler()

def update(dt):
    screen_handler.generate_events()
    screen_handler.ai_update()
    screen_handler.update(dt)
 
@screen_handler.game_window.event
def on_draw():
    screen_handler.game_window.clear()
    screen_handler.screen.on_draw()
    screen_handler.main_batch.draw()

@screen_handler.game_window.event
def on_key_press(symbol, modifiers):
    screen_handler.keyboard_cont.handle_keys(True, symbol, modifiers)

@screen_handler.game_window.event
def on_key_release(symbol, modifiers):
    screen_handler.keyboard_cont.handle_keys(False, symbol, modifiers)
    
@screen_handler.game_window.event
def on_mouse_press(x, y, button, modifiers):
    screen_handler.mouse_cont.handle_mouse(True, x,y,button,modifiers)

@screen_handler.game_window.event
def on_mouse_release(x, y, button, modifiers):
    screen_handler.mouse_cont.handle_mouse(False, x,y,button,modifiers)

@screen_handler.game_window.event
def on_mouse_drag(x, y, dx, dy, button, modifiers):
    screen_handler.mouse_cont.handle_mouse(True, x,y,button,modifiers)


if __name__ == '__main__':
    pyglet.clock.schedule_interval(update, 1/120.0)
    pyglet.app.run()