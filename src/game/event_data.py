'''
Created on 6 jul 2013

@author: Petter
'''
from weakref import WeakKeyDictionary

class EventManager:
    def __init__(self):
        self.listeners = WeakKeyDictionary()
        self.eventQueue= []

    def reg_listener(self, listener):
        self.listeners[listener] = 1

    def unreg_listener(self, listener):
        if listener in self.listeners:
            del self.listeners[listener]

    def post(self, event):
        #if event.name=='Char Walk Request':
        #    print("     Message: " + event.name)
        for listener in self.listeners:
            listener.notify(event)

class Event():
    def __init__(self):
        self.name = 'Generic Event'

class GameStartRequest(Event):
    def __init__(self):
        self.name = 'Game Start Request'

class CharWalkRequest(Event):
    def __init__(self, direction):
        self.name = 'Char Walk Request'
        self.direction = direction

class CharStopwalkRequest(Event):
    def __init__(self):
        self.name = 'Char Stop Walk Request'   

class CharJumpRequest(Event):
    def __init__(self, height, forced):
        self.name = 'Char Jump Request'
        self.height = height
        self.forced = forced

class CharUnjumpRequest(Event):
    def __init__(self):
        self.name = 'Char Unjump Request'

class GoToMenuRequest(Event):
    def __init__(self):
        self.name = 'Go To Menu Request'

class PrintDebugRequest(Event):
    def __init__(self):
        self.name = 'Print Debug Request'

class MousePress(Event):
    def __init__(self,x,y,button,modifier):
        self.name = 'Mouse Press'
        self.x = x
        self.y = y
        self.button = button
        self.modifier = modifier

class MouseRelease(Event):
    def __init__(self,x,y,button,modifier):
        self.name = 'Mouse Release'
        self.x = x
        self.y = y
        self.button = button
        self.modifier = modifier

class BulletFired(Event):
    def __init__(self,x,y,width,height,tile_data,x_vel,y_vel):
        self.name = 'Bullet Fired'
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.tile_data = tile_data
        self.x_vel = x_vel
        self.y_vel = y_vel