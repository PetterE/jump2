'''
Created on 15 jul 2013

@author: Petter
'''
import resources, physicalobject
from util import overlap

class Bullet(physicalobject.PhysicalObject):
    def __init__(self, abs_x, abs_y, width, height, tile_data, velocity_x, velocity_y, *args, **kwargs):
        super(Bullet, self).__init__(abs_x, abs_y, width, height, tile_data, resources.bullet_image, *args, **kwargs)
        self.tile_data = tile_data
        self.velocity_x, self.velocity_y = velocity_x, velocity_y
        self.start_x = self.x
        self.start_y = self.y
        self.abs_x = abs_x
        self.abs_y = abs_y
        self.obj_width = width
        self.obj_height = height
        self.hitbox = [self.abs_x,self.abs_y,self.abs_x+self.obj_width,self.abs_y+self.obj_height]
        self.tagged_for_removal = False

    def update(self,dt):
        self.x_movement(dt)
        self.y_movement(dt)
        self.gravity(dt)
        self.hitbox = [
            self.abs_x,                 #0 left
            self.abs_y,                 #1 bottom
            self.abs_x+self.obj_width,  #2 right
            self.abs_y+self.obj_height  #3 top
            ]
        if self.check_bounds():
            self.tagged_for_removal = True

    def x_movement(self,dt):
        closest_tile = None
        for tile in self.tile_data.tiles:
            if overlap(self.hitbox[1],self.hitbox[3],tile[1],tile[1]+self.tile_data.tile_side): # not fully outside tile y-space
                # find closest tile in appropriate direction
                if self.velocity_x>0: 
                    if tile[0]>self.hitbox[2]:
                        if closest_tile is None:
                            closest_tile = tile[0]
                        closest_tile = min(closest_tile,tile[0])
                if self.velocity_x<0:
                    if tile[0]+self.tile_data.tile_side<self.hitbox[0]:
                        closest_tile = max(closest_tile,tile[0]+self.tile_data.tile_side)
        if closest_tile is None: # no colliding tile in direction, just move as much as wanted
            self.abs_x = self.abs_x+(self.velocity_x*dt)
        elif self.velocity_x>0:
            closest_tile = closest_tile-self.obj_width-1
            self.abs_x = min(self.abs_x+(self.velocity_x*dt),closest_tile)
        elif self.velocity_x<0:
            closest_tile += 1
            self.abs_x = max(self.abs_x+(self.velocity_x*dt),closest_tile)
        if self.abs_x==closest_tile:
            self.tagged_for_removal = True
            
    def y_movement(self,dt):
        closest_tile = None
        for tile in self.tile_data.tiles:
            if overlap(self.hitbox[0],self.hitbox[2],tile[0],tile[0]+self.tile_data.tile_side): # not fully outside tile x-space
                # find closest tile in appropriate direction
                if self.velocity_y>0: 
                    if tile[1]>self.hitbox[3]:
                        if closest_tile is None:
                            closest_tile = tile[1]
                        closest_tile = min(closest_tile,tile[1])
                if self.velocity_y<0:
                    if tile[1]+self.tile_data.tile_side<self.hitbox[1]: # using <, but placing player 1 pixel above "roof" of tile
                        closest_tile = max(closest_tile,tile[1]+self.tile_data.tile_side)
        if closest_tile is None: # no colliding tile in direction, just move as much as wanted
            self.abs_y = self.abs_y+(self.velocity_y*dt)
        elif self.velocity_y>0:
            closest_tile = closest_tile-self.obj_height-1 # -1 because I want to place player below the bottom pixel of the tile
            self.abs_y = min(self.abs_y+(self.velocity_y*dt),closest_tile)
            if self.abs_y == closest_tile: # hit roof
                self.velocity_y = 0
        elif self.velocity_y<0:
            closest_tile += 1 # +1 because I want to place player above the top pixel of the tile
            self.abs_y = max(self.abs_y+(self.velocity_y*dt),closest_tile) 
            if self.abs_y == closest_tile: # standing on floor
                self.velocity_y = 0
        if self.abs_y==closest_tile:
            self.tagged_for_removal = True 