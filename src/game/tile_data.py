'''
Created on 16 jun 2013

@author: Petter

'''
import csv

class TileData():
    def __init__(self, level):
        self.tile_side = 20
        self.tile_map = []
           
        if level==0: # Main menu "level"
            self.left_wall = 0
            self.right_wall = 800
            self.bottom_wall = 0
            self.top_wall = 600
            self.tiles = []
            self.file = open('levels/menu_tiles.csv', 'r')
                               
        if level==1:
            self.left_wall = -800
            self.right_wall = 3000
            self.bottom_wall = 0
            self.top_wall = 3000
            self.tiles = []
            self.file = open('levels/lvl1tiles.csv', 'r')
        
        self.csv_level = csv.reader(self.file, delimiter=';')
                
        for row in self.csv_level:
            self.tile_map.append(row)
                
        self.tile_map.reverse()
                
        for y,tile_row in enumerate(self.tile_map):
            for x,bit in enumerate(tile_row):
                bit = int(bit)
                if bit==1:
                    self.tiles.append([x*20,y*20,'test'])
                elif bit==2:
                    self.tiles.append([x*20,y*20,'bridge'])

        self.file.close()