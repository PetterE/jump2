'''
Created on 16 jun 2013

@author: Petter
''' 
import physicalobject, resources, event_data
from util import get_bullet_velocity

class Player(physicalobject.PhysicalObject):
    def __init__(self, abs_x, abs_y, width, height, tile_data, evManager, *args, **kwargs):
        self.stand_image_left = resources.player_stand_left_image
        self.stand_image_right = resources.player_stand_right_image
        self.walk_ani_left = resources.player_walk_left_ani
        self.walk_ani_right = resources.player_walk_right_ani
        self.hitstun_image = resources.player_hit_ani
        self.evManager = evManager
        super(Player, self).__init__(abs_x, abs_y, width, height, tile_data, resources.player_stand_left_image, *args, **kwargs)
        self.facing_left = True
        self.walk_change = False
        self.is_walking = False
        self.in_hitstun = False
        self.charge_throw = False
        self.throw_charge = 20
        self.hitstun_counter = 0
        self.health = 10
        
    def notify(self, event):
        if not self.in_hitstun:
            if event.name=='Char Walk Request':
                if self.velocity_x != event.direction: # if there's a change
                    self.velocity_x = event.direction
                    self.walk_change = True
                    self.is_walking = True
                    if event.direction<0:
                        self.facing_left = True
                    else:
                        self.facing_left = False
            elif event.name=='Char Stop Walk Request':
                self.velocity_x = 0
                self.walk_change = True
                self.is_walking = False
            elif event.name=='Char Jump Request':
                self.jump(event.height, event.forced)
            elif event.name=='Char Unjump Request':
                self.unjump()
            self.update_animation()
        if event.name=='Mouse Press':
            self.charge_throw = True
        if event.name=='Mouse Release':
            self.charge_throw = False
            x_vel, y_vel = get_bullet_velocity(self.throw_charge*30, self.x,self.y,event.x,event.y)
            self.evManager.post(event_data.BulletFired(self.abs_x+self.width/2,self.abs_y+self.height/2,10,10,self.tile_data,x_vel,y_vel))            
            self.throw_charge = 20
        
    def update(self, dt):
        super(Player, self)._update(dt)
        if self.in_hitstun: # Kind of strange that the duration of hitstun only depends on runs of update loop, should use dt...
            self.hitstun_counter -= 1
            if self.hitstun_counter == 0:
                self.in_hitstun = False
                self.image = self.stand_image_left
        if self.charge_throw:
            self.throw_charge += 1
            self.throw_charge = min(self.throw_charge,50)
            print self.throw_charge
    
    def update_animation(self):
        if self.walk_change:
            self.walk_change = False
            if self.is_walking:
                if self.facing_left:
                    self.image = self.walk_ani_left
                else:
                    self.image = self.walk_ani_right
            elif not self.is_walking:
                if self.facing_left:
                    self.image = self.stand_image_left
                else:
                    self.image = self.stand_image_right       
    
    def hitstun(self,vel_x,vel_y,duration):
        self.is_walking = False
        self.in_hitstun = True
        self.velocity_x = vel_x
        self.velocity_y = vel_y
        self.hitstun_counter = duration
        self.image = self.hitstun_image
        
    def handle_collision_with(self, obj):
        if self.check_collision_with(obj):
            if self.hitbox[1]>obj.hitbox[3]-obj.height/2 and not self.in_hitstun: #feet above 2/4 of objs height, jumped on obj, velocity thing good enough for smb
                self.jump(800,True)
                return 'kill'
            elif not self.hitbox[1]>obj.hitbox[3]-obj.height/2 and not self.in_hitstun: #feet not above 2/4 of objs height, collided with obj
                #...take damage, hit movement/animation
                resources.test_hurt_sound.play()
                self.health -= 1
                if self.hitbox[2]-self.width/2>obj.hitbox[2]-obj.width/2: # player middle to the right of middle of obj, move right if true
                    self.hitstun(200,250,20)
                else:
                    self.hitstun(-200,250,20)
                return 'hit'
            else:
                return 'hit in hitstun'
        else:
            return 'no collision'
                
        

            
    