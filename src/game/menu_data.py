'''
Created on 8 jul 2013

@author: Petter
'''
import pyglet, random
import resources, alien_enemy, tile_data

class Dead():
    def __init__(self, screenhandler):
        self.screenhandler = screenhandler
        self.starting = False
        self.screenhandler.game_objects.append(self)
        screenhandler.event_manager.reg_listener(self)
        
    def on_draw(self):
        resources.dead_menu_image.blit(0,0) # @UndefinedVariable   

    def notify(self, event):
        if event.name=='Game Start Request' and not self.starting:
            self.starting = True
            pyglet.clock.schedule_once(self.screenhandler.go_to_screen, 0.2, 1)
        if event.name=='Print Debug Request':
            pass  

class MainMenu():
    def __init__(self, screenhandler):
        self.screenhandler = screenhandler
        self.starting = False
        self.screenhandler.game_objects.append(self)
        self.bg_ticker = 0
        self.wait_ticker = 0
        self.tile_data = tile_data.TileData(0)
        for _ in range(3):
            self.screenhandler.game_objects.append(alien_enemy.AlienEnemy(random.randint(200,600),random.randint(650,1000),40,40,self.tile_data,random.randint(-400,-200),0,800, x=200, y=300, batch=self.screenhandler.main_batch))
        screenhandler.event_manager.reg_listener(self)
        

    def on_draw(self):
        resources.main_menu_image.blit(0,-600+self.bg_ticker) # @UndefinedVariable
        self.wait_ticker += 1
        if self.bg_ticker<600 and self.wait_ticker>800:
            self.bg_ticker += 0.2

    def update(self, dt): # Needed because we handle this in camera_movement() ordinarily, this is a special case for nice effect on main menu
        for enemy in filter(lambda x: isinstance(x, alien_enemy.AlienEnemy), self.screenhandler.game_objects):
            enemy.x = enemy.abs_x
            enemy.y = enemy.abs_y
    
    def notify(self, event):
        if event.name=='Game Start Request' and not self.starting:
            self.starting = True
            pyglet.clock.schedule_once(self.screenhandler.go_to_screen, 0.2, 1)
        if event.name=='Print Debug Request':
            for obj in self.screenhandler.game_objects:
                print obj    