'''
Created on 6 jul 2013

@author: Petter
'''
from pyglet.window import key
import event_data

class InputController:
    def __init__(self, evManager, level):
        self.evManager = evManager
        self.level = level
        

class KeyboardController(InputController):
    def __init__(self, evManager, level):
        InputController.__init__(self, evManager, level)
        self.keys = dict()

    def handle_keys(self, pressed, symbol, modifiers):
        self.keys[symbol] = pressed

    def generate_events(self):
        events = []
        if self.level <= 0:
            if self.keys.get(key.ENTER):
                events.append(event_data.GameStartRequest())
        elif self.level == 1:
            if self.keys.get(key.A):
                events.append(event_data.CharWalkRequest(-400))
            elif self.keys.get(key.D):
                events.append(event_data.CharWalkRequest(400))
            else:
                events.append(event_data.CharStopwalkRequest())           
            if self.keys.get(key.W):
                events.append(event_data.CharJumpRequest(1200,False))
            elif not self.keys.get(key.W):
                events.append(event_data.CharUnjumpRequest())
            
            if self.keys.get(key.ENTER):
                events.append(event_data.GoToMenuRequest())
        if self.keys.get(key.SPACE):
            events.append(event_data.PrintDebugRequest())               
        
        for event in events:
            self.evManager.post(event)

class MouseController(InputController):
    def __init__(self, evManager, level):
        InputController.__init__(self, evManager, level)
        self.mousebuttons = dict()
    
    def handle_mouse(self, pressed, x, y, button, modifiers):
        self.mousebuttons[button] = MouseButton(pressed,x,y,modifiers)
    
    def generate_events(self):
        for button,value in self.mousebuttons.iteritems():
            if value.pressed and not value.is_posted:
                self.evManager.post(event_data.MousePress(value.x,value.y,button,value.modifiers))
                value.is_posted = True
            if not value.pressed and not value.is_posted:
                self.evManager.post(event_data.MouseRelease(value.x,value.y,button,value.modifiers))
                value.is_posted = True
                
class MouseButton:
    def __init__(self, pressed, x, y, modifiers):
        self.pressed = pressed
        self.x = x
        self.y = y
        self.modifiers = modifiers
        self.is_posted = False