'''
Created on 16 jun 2013

@author: Petter
'''
import pyglet, resources
from util import overlap, box_collide

class PhysicalObject(pyglet.sprite.Sprite):
    def __init__(self, abs_x, abs_y, width, height, tile_data, *args, **kwargs):
        super(PhysicalObject, self).__init__(*args, **kwargs)
        self.tile_data = tile_data
        self.velocity_x, self.velocity_y = 0.0, 0.0
        self.floored = True
        self.forced_jump = False
        self.floor_counter = 0
        self.start_x = self.x
        self.start_y = self.y
        self.abs_x = abs_x
        self.abs_y = abs_y
        self.obj_width = width
        self.obj_height = height
        self.hitbox = [self.abs_x,self.abs_y,self.abs_x+self.obj_width,self.abs_y+self.obj_height]
        self.tagged_for_removal = False

    def _update(self, dt):
        if self.floor_counter>3: # Should use dt
            self.floored = True
        self.gravity(dt)
        self.x_movement(dt)
        self.y_movement(dt)
        if self.velocity_y<0: # If a fall starts
            self.floored = False
        self.check_bounds()
        self.hitbox = [
            self.abs_x,                 #0 left
            self.abs_y,                 #1 bottom
            self.abs_x+self.obj_width,  #2 right
            self.abs_y+self.obj_height  #3 top
            ]
        
    def x_movement(self,dt):
        if self.velocity_x==0: # skip this if not moving (x-wise)
            return
        closest_tile = None
        for tile in self.tile_data.tiles:
            if overlap(self.hitbox[1],self.hitbox[3],tile[1],tile[1]+self.tile_data.tile_side): # not fully outside tile y-space
                # find closest tile in appropriate direction
                if self.velocity_x>0: 
                    if tile[0]>self.hitbox[2]:
                        if closest_tile is None:
                            closest_tile = tile[0]
                        closest_tile = min(closest_tile,tile[0])
                if self.velocity_x<0:
                    if tile[0]+self.tile_data.tile_side<self.hitbox[0]:
                        closest_tile = max(closest_tile,tile[0]+self.tile_data.tile_side)
        if closest_tile is None: # no colliding tile in direction, just move as much as wanted
            self.abs_x = self.abs_x+(self.velocity_x*dt)
        elif self.velocity_x>0:
            closest_tile = closest_tile-self.obj_width-1
            self.abs_x = min(self.abs_x+(self.velocity_x*dt),closest_tile)
        elif self.velocity_x<0:
            closest_tile += 1
            self.abs_x = max(self.abs_x+(self.velocity_x*dt),closest_tile)

    def y_movement(self,dt):
        closest_tile = None
        for tile in self.tile_data.tiles:
            if overlap(self.hitbox[0],self.hitbox[2],tile[0],tile[0]+self.tile_data.tile_side): # not fully outside tile x-space
                # find closest tile in appropriate direction
                if self.velocity_y>0: 
                    if tile[1]>self.hitbox[3]:
                        if closest_tile is None:
                            closest_tile = tile[1]
                        closest_tile = min(closest_tile,tile[1])
                if self.velocity_y<0:
                    if tile[1]+self.tile_data.tile_side<self.hitbox[1]: # using <, but placing player 1 pixel above "roof" of tile
                        closest_tile = max(closest_tile,tile[1]+self.tile_data.tile_side)
        if closest_tile is None: # no colliding tile in direction, just move as much as wanted
            self.abs_y = self.abs_y+(self.velocity_y*dt)
        elif self.velocity_y>0:
            closest_tile = closest_tile-self.obj_height-1 # -1 because I want to place player below the bottom pixel of the tile
            self.abs_y = min(self.abs_y+(self.velocity_y*dt),closest_tile)
            if self.abs_y == closest_tile: # hit roof
                self.velocity_y = 0
        elif self.velocity_y<0:
            closest_tile += 1 # +1 because I want to place player above the top pixel of the tile
            self.abs_y = max(self.abs_y+(self.velocity_y*dt),closest_tile) 
            if self.abs_y == closest_tile: # standing on floor
                self.floor_counter += 1
                self.velocity_y = 0
    
    def gravity(self, dt):
        if self.velocity_y>-1200:
            if self.velocity_y<-600:
                self.velocity_y += ((self.velocity_y+1200)/-15)*(dt/0.014)
            elif self.velocity_y<80 and self.velocity_y>0:
                self.velocity_y -= 10*(dt/0.014)
            else:
                self.velocity_y -= 40*(dt/0.014)

    def jump(self,height,force_jump):
        if self.floored or force_jump: #can't jump when falling or jumping, will allow to force some jumps
            self.velocity_y = height
            self.floored = False
            self.floor_counter = 0
            resources.test_jump_sound.play()
            self.forced_jump = force_jump
            return True
        else:
            return False

    def unjump(self):
        if self.velocity_y > 0 and not self.forced_jump: #can't unjump when falling or standing
            self.velocity_y = 0
            
    def check_collision_with(self, other_object):
        if box_collide(self.hitbox,other_object.hitbox):
            return True
        else:
            return False
    
    def check_bounds(self):
        if self.hitbox[3]<self.tile_data.bottom_wall or self.hitbox[0]<self.tile_data.left_wall or self.hitbox[2]>self.tile_data.right_wall or self.hitbox[1]>self.tile_data.top_wall:
            return True
        else:
            return False
