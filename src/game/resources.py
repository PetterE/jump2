'''
Created on 16 jun 2013

@author: Petter
'''
import pyglet

# Tell pyglet where to find the resources
pyglet.resource.path = ['resources']
pyglet.resource.reindex()

def center_image(image):
    image.anchor_x = image.width/2
    image.anchor_y = image.height/2

def offset_anchor(image, x,y):
    image.anchor_x = image.anchor_x+x
    image.anchor_y = image.anchor_y+y  

# menu resources
main_menu_image = pyglet.resource.image("main_menu.png")
dead_menu_image = pyglet.resource.image("dead_menu.png")

# lvl one resources
background_front_image = pyglet.resource.image("background_front.png")
background_mid_image = pyglet.resource.image("background_mid.png")
background_back_image = pyglet.resource.image("background_back.png")
goal_image = pyglet.resource.image("goal.png")

test_tile_image = pyglet.resource.image("testtile.png")
bridge_tile_image = pyglet.resource.image("bridgetile.png")

player_stand_left_image = pyglet.resource.image("player_stand_left.png")
player_stand_right_image = pyglet.resource.image("player_stand_right.png")
player_walk1_left_image = pyglet.resource.image("player_walk1_left.png")
player_walk1_right_image = pyglet.resource.image("player_walk1_right.png")
player_walk2_left_image = pyglet.resource.image("player_walk2_left.png")
player_walk2_right_image = pyglet.resource.image("player_walk2_right.png")
player_walk_left_ani = pyglet.image.Animation.from_image_sequence([player_walk1_left_image,player_walk2_left_image], 0.2, loop=True)
player_walk_right_ani = pyglet.image.Animation.from_image_sequence([player_walk1_right_image,player_walk2_right_image], 0.2, loop=True)
player_hit_solid = pyglet.resource.image("player_hit_solid.png")
player_hit_trans = pyglet.resource.image("player_hit_trans.png")
player_hit_ani = pyglet.image.Animation.from_image_sequence([player_hit_solid,player_hit_trans], 0.2, loop=True)

mouse_test_image = pyglet.resource.image("mousetest.png")
center_image(mouse_test_image)
bullet_image = pyglet.resource.image('bullet.png')

enemy_image = pyglet.resource.image("enemy.png")
offset_anchor(enemy_image,1,1)

health_bar_frame = pyglet.resource.image("health_bar_frame.png")
health_bar_fill = pyglet.resource.image("health_bar_fill.png")
kill_effect_image1 = pyglet.resource.image("kill_effect1.png")
center_image(kill_effect_image1)
kill_effect_image2 = pyglet.resource.image("kill_effect2.png")
center_image(kill_effect_image2)

test_kill_sound = pyglet.resource.media('kill.wav', streaming=False)
test_jump_sound = pyglet.resource.media('jump_test.wav', streaming=False)
test_hurt_sound = pyglet.resource.media('hurt.wav', streaming=False)
test_song_sound = pyglet.resource.media('song1.wav')
#test_song_sound.play()



