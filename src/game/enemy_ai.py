'''
Created on 7 jul 2013

@author: Petter
'''
import alien_enemy
import random

class EnemyAI():
    def __init__(self, screenhandler):
        self.screenhandler = screenhandler
        
    # Note: not actually generating events here, rather calling the "notify" function of each enemy directly
    # The alternative is to give all enemies a number and use that in event generation, but we'd spam the event queue heavily
    def ai_update(self):
        for enemy in filter(lambda x: isinstance(x, alien_enemy.AlienEnemy), self.screenhandler.game_objects):
            if not enemy.scripted_jump: #don't wanna ruin the scripted jump with a random one
                self._random_jump_ai(enemy)
            self._random_follow_player_ai(enemy, self.screenhandler.screen)
            self._check_bounds_ai(enemy)
            if enemy.scripted_jump:
                enemy.scripted_jump = [jumpdata for jumpdata in enemy.scripted_jump if self._scripted_jump(enemy, jumpdata)]
                    
    def _random_jump_ai(self,enemy):
        # Jump sometimes
        if random.randint(1,300)>299:
            enemy.jump(random.randint(200,300),False)
    
    def _random_follow_player_ai(self,enemy,screen):
        # Change direction sometimes, need some handling in order not to crash if there is no player initialized on the screen
        player_x = None
        if hasattr(screen, 'player_dude'):
            player_x = screen.player_dude.abs_x
        else:
            player_x = 400
            
        if random.randint(1,40)>39:
            if player_x > enemy.abs_x:
                enemy.velocity_x = abs(enemy.velocity_x)
            else:
                enemy.velocity_x = abs(enemy.velocity_x)*-1
    
    def _check_bounds_ai(self,enemy):
        if enemy.velocity_x<0 and enemy.left_x_bound>enemy.hitbox[0] and enemy.left_x_bound!=None:
            enemy.velocity_x = enemy.velocity_x*-1
        elif enemy.velocity_x>0 and enemy.right_x_bound<enemy.hitbox[2] and enemy.right_x_bound!=None:
            enemy.velocity_x = enemy.velocity_x*-1
    
    def _scripted_jump(self,enemy,jumpdata):
        if jumpdata[0]=='left' and jumpdata[1]>enemy.hitbox[0]:
            if enemy.jump(jumpdata[2],False):
                return False
            else:
                return True
        elif jumpdata[0]=='right' and jumpdata[1]<enemy.hitbox[2]:
            if enemy.jump(jumpdata[2],False):
                return False
            else:
                return True
        else:
            return True                                  