'''
Created on 30 jun 2013

@author: Petter
'''
import pyglet
import event_data, enemy_ai, level_data, menu_data, input_controllers
from pyglet.gl import *

class ScreenHandler():
    def __init__(self):
        self.screen_x = 800
        self.screen_y = 600
        self.game_window = pyglet.window.Window(self.screen_x, self.screen_y)
        # These two functions needs to be run after the window is initialized on some setups
        glEnable(GL_BLEND) 
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA) 
        self.main_batch = pyglet.graphics.Batch()
        self.screen = None
        self.hiscore = 0
        
        # Initialising event manager, keyboard controller and enemy AI
        self.event_manager = event_data.EventManager()
        self.keyboard_cont = input_controllers.KeyboardController(self.event_manager, None)
        self.mouse_cont = input_controllers.MouseController(self.event_manager, None)
        self.enemy_ai = enemy_ai.EnemyAI(self)
        
        # Keeping a list of event-generators, will basically only contain a single keyboard controller
        # Will run through list as first step of main loop, running generate_events()
        self.event_generators = []
        self.event_generators.append(self.keyboard_cont)
        self.event_generators.append(self.mouse_cont)
        
        # Keeping a list of AI classes
        # Will run through list as second step of main loop, running ai_update()
        self.ai_updaters = []
        self.ai_updaters.append(self.enemy_ai)
        
        # Keeping a game object list
        # Will run through list as third step of main loop, running their update() function if they have one
        self.game_objects = []
        self.go_to_screen(1, 0)
        
        
    def clean_screens(self):
        #Delete objects that have a delete method
        for obj in self.game_objects:
            try:
                obj.delete()
            except AttributeError:
                pass
        self.game_objects = []
        self.game_window.remove_handlers()

    def go_to_screen(self, dt, level):
        self.clean_screens()
        self.keyboard_cont.level = level
        if level==-1:
            self.screen = menu_data.Dead(self)
        elif level==0:
            self.screen = menu_data.MainMenu(self)         
        elif level==1:
            self.screen = level_data.LevelOne(self)

    def generate_events(self):
        for obj in self.event_generators:
            obj.generate_events()
    
    def ai_update(self):
        for ai in self.ai_updaters:
            ai.ai_update()
    
    def update(self, dt):
        for obj in self.game_objects:
            try:
                obj.update(dt)
            except AttributeError:
                pass
        to_be_removed = []
        for idx, obj in enumerate(self.game_objects):
            try:
                if obj.tagged_for_removal==True:
                    to_be_removed.append(idx)
            except AttributeError:
                pass
        to_be_removed.sort()
        to_be_removed.reverse()
        for i in to_be_removed:
            self.game_objects.pop(i).delete()                  
                    
    

          
        
      
