'''
Created on 4 jul 2013

@author: Petter
'''
import physicalobject, resources

class AlienEnemy(physicalobject.PhysicalObject):
    def __init__(self, abs_x, abs_y, width, height, tile_data, velocity_x, left_x_bound=None, right_x_bound=None, scripted_jump=None, *args, **kwargs):
        super(AlienEnemy, self).__init__(abs_x, abs_y, width, height, tile_data, resources.enemy_image, *args, **kwargs)
        self.velocity_x = velocity_x
        self.left_x_bound = left_x_bound
        self.right_x_bound = right_x_bound
        self.scripted_jump = scripted_jump
        
    def update(self,dt):
        super(AlienEnemy, self)._update(dt)
