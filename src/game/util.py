'''
Created on 16 jun 2013

@author: Petter
'''
import math

def distance(point_1=(0, 0), point_2=(0, 0)):
    return math.sqrt((point_1[0]-point_2[0])**2+(point_1[1]-point_2[1])**2)

def overlap(min1,max1,min2,max2):
    if min1>max2:
        return False
    elif max1<min2:
        return False
    else:
        return True

def box_collide(box1, box2):
    if overlap(box1[0],box1[2],box2[0],box2[2]) and overlap(box1[1],box1[3],box2[1],box2[3]):
        return True
    else:
        return False

def get_bullet_velocity(speed, x1,y1,x2,y2):
    width  = float(x2-x1)
    height = float(y2-y1)
    angle = math.atan2(height, width)
    x_vel = math.cos(angle)*speed
    y_vel = math.sin(angle)*speed
    return x_vel, y_vel