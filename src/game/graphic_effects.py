'''
Created on 8 jul 2013

@author: Petter
'''
import resources

class KillEffect():
    def __init__(self,abs_x,abs_y,duration):
        self.abs_x = abs_x
        self.abs_y = abs_y
        self.duration = duration
        if int(abs_x)%2==0:
            self.image = resources.kill_effect_image1
        else:
            self.image = resources.kill_effect_image2

class MousePressEffect():
    def __init__(self,abs_x,abs_y,duration):
        self.abs_x = abs_x
        self.abs_y = abs_y
        self.duration = duration
        self.image = resources.mouse_test_image
