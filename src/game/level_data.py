'''
Created on 7 jul 2013

@author: Petter
'''

import pyglet
import resources, alien_enemy, player, tile_data, graphic_effects, bullet
from util import get_bullet_velocity, box_collide

class TopLevel():
    def __init__(self, screenhandler, level):
        self.screenhandler = screenhandler
        # Add the level to game objects
        self.screenhandler.game_objects.append(self)
        
        # Build variables needed by all levels
        self.camera_position_x = 0
        self.camera_position_y = 0
        self.bg_ticker = 0
        self.score = 0
        self.ending_level = False
        self.going_to_menu = False
        self.triggers = dict()

        # Score and level labels
        self.score_label = pyglet.text.Label(text='Score: '+str(self.score)+' Best: '+str(self.screenhandler.hiscore), x=10, y=575, batch=self.screenhandler.main_batch)
        self.level_label = pyglet.text.Label(text='Jump2', x=400, y=575, anchor_x='center', batch=self.screenhandler.main_batch)
        self.screenhandler.game_objects.append(self.score_label)
        self.screenhandler.game_objects.append(self.level_label)
        
        # Effects
        self.graphic_effects = []

        # Get tile data for level
        self.tile_data = tile_data.TileData(level)

    def on_draw(self):
        self.camera_movement()
        self.draw_background(self.bg_ticker)
        self.draw_health_bar()
        self.draw_effects()
        self.draw_tiles()       

    def camera_movement(self):
        self.camera_position_x = self.player_dude.abs_x-self.player_dude.start_x
        self.camera_position_y = self.player_dude.abs_y-self.player_dude.start_y
        # Wall collision x
        if self.camera_position_x<self.tile_data.left_wall: # closer to left wall than we want the camera to follow
            self.camera_position_x = self.tile_data.left_wall
            self.player_dude.x = self.player_dude.abs_x-self.tile_data.left_wall
        elif self.camera_position_x+self.screenhandler.screen_x>self.tile_data.right_wall: # closer to right wall than we want the camera to follow
            self.camera_position_x = self.tile_data.right_wall-self.screenhandler.screen_x
            self.player_dude.x = self.player_dude.abs_x-self.tile_data.right_wall+self.screenhandler.screen_x
        else: # Else, go back to start x to avoid rounding problems
            self.player_dude.x = self.player_dude.start_x 
        # Wall collision y
        if self.camera_position_y<self.tile_data.bottom_wall: # closer to bottom wall than we want the camera to follow
            self.camera_position_y = self.tile_data.bottom_wall
            self.player_dude.y = self.player_dude.abs_y-self.tile_data.bottom_wall
        elif self.camera_position_y+self.screenhandler.screen_y>self.tile_data.top_wall: # closer to top wall than we want the camera to follow
            self.camera_position_y = self.tile_data.top_wall-self.screenhandler.screen_y
            self.player_dude.y = self.player_dude.abs_y-self.tile_data.top_wall+self.screenhandler.screen_y        
        else: # Else, go back to start y to avoid rounding problems
            self.player_dude.y = self.player_dude.start_y
        # This is ugly, how to clean?
        for physob in filter(lambda x: isinstance(x,alien_enemy.AlienEnemy) or isinstance(x,bullet.Bullet), self.screenhandler.game_objects): #filter out non-player sprites:
            physob.x = physob.abs_x-self.camera_position_x
            physob.y = physob.abs_y-self.camera_position_y

    def draw_background(self, bg_ticker):
        resources.background_back_image.blit(self.tile_data.left_wall-(self.camera_position_x/8)-bg_ticker+self.screenhandler.screen_x*0,0-(self.camera_position_y/8))  # @UndefinedVariable
        resources.background_back_image.blit(self.tile_data.left_wall-(self.camera_position_x/8)-bg_ticker+self.screenhandler.screen_x*1,0-(self.camera_position_y/8))  # @UndefinedVariable
        resources.background_back_image.blit(self.tile_data.left_wall-(self.camera_position_x/8)-bg_ticker+self.screenhandler.screen_x*2,0-(self.camera_position_y/8))  # @UndefinedVariable
        resources.background_mid_image.blit(self.tile_data.left_wall-(self.camera_position_x/4),0-(self.camera_position_y/8))  # @UndefinedVariable
        resources.background_mid_image.blit(self.tile_data.left_wall+self.screenhandler.screen_x-(self.camera_position_x/4),0-(self.camera_position_y/8))  # @UndefinedVariable
        resources.background_front_image.blit(self.tile_data.left_wall-(self.camera_position_x/3),0-(self.camera_position_y/8))  # @UndefinedVariable
        resources.background_front_image.blit(self.tile_data.left_wall+self.screenhandler.screen_x-(self.camera_position_x/3),0-(self.camera_position_y/8))  # @UndefinedVariable
        resources.goal_image.blit(2540-self.camera_position_x,301-self.camera_position_y) # @UndefinedVariable
        self.bg_ticker += 1
        if self.bg_ticker>self.screenhandler.screen_x:
            self.bg_ticker = 0
            
    def draw_health_bar(self):
        resources.health_bar_frame.blit(30,540) # @UndefinedVariable
        for i in range(self.player_dude.health):
            resources.health_bar_fill.blit(34+i*15,544) # @UndefinedVariable        

    def draw_tiles(self):
        for tile in self.tile_data.tiles:
            tile_rel_x = tile[0]-int(self.camera_position_x)
            tile_rel_y = tile[1]-int(self.camera_position_y)
            if tile_rel_x>=-20 and tile_rel_x<self.screenhandler.screen_x+20 and tile_rel_y>=-20 and tile_rel_y<self.screenhandler.screen_y+20:
                if tile[2]=='test':
                    resources.test_tile_image.blit(tile_rel_x,tile_rel_y)  # @UndefinedVariable
                elif tile[2]=='bridge':
                    resources.bridge_tile_image.blit(tile_rel_x,tile_rel_y)  # @UndefinedVariable

    def draw_effects(self):
        for effect in self.graphic_effects:
            effect.image.blit(effect.abs_x-self.camera_position_x,effect.abs_y-self.camera_position_y) # @UndefinedVariable        
            effect.duration -= 1
        self.graphic_effects = [effect for effect in self.graphic_effects if effect.duration>0]

class LevelOne(TopLevel):
    def __init__(self, screenhandler):
        TopLevel.__init__(self, screenhandler, 1)
        
        # Instancing the player and enemies 
        self.player_dude = player.Player(20,21,30,60, self.tile_data, self.screenhandler.event_manager, x=400, y=200, batch=self.screenhandler.main_batch)
        self.screenhandler.game_objects.append(self.player_dude)
        self.screenhandler.game_objects.append(alien_enemy.AlienEnemy(460,221,40,40,self.tile_data,-300, 360, 540, x=200, y=300, batch=self.screenhandler.main_batch))
        self.screenhandler.game_objects.append(alien_enemy.AlienEnemy(120,21,40,40,self.tile_data,-60, 0, 360, x=200, y=300, batch=self.screenhandler.main_batch))
        self.screenhandler.game_objects.append(alien_enemy.AlienEnemy(1220,381,40,40,self.tile_data,-100, 1200, 1260, x=200, y=300, batch=self.screenhandler.main_batch))
        
        # Push player and level as event listeners
        screenhandler.event_manager.reg_listener(self.player_dude)
        screenhandler.event_manager.reg_listener(self)
        
        # Adding triggers to dict
        for i in range(4):
            self.triggers[i] = False

    def update(self, dt):
        self._handle_collisions()
        self._handle_triggers()        
        # Dead
        if (self.player_dude.check_bounds() or self.player_dude.health<=0) and not self.ending_level:
            self.ending_level = True
            self.screenhandler.go_to_screen(0,-1)
    
    def _handle_triggers(self):
        if self.player_dude.abs_x>40 and not self.triggers[0]:
            self.triggers[0] = True
            self.screenhandler.game_objects.append(alien_enemy.AlienEnemy(550,221,40,40,self.tile_data,-350, 0, 360, x=200, y=300, batch=self.screenhandler.main_batch))
            self.screenhandler.game_objects.append(alien_enemy.AlienEnemy(500,221,40,40,self.tile_data,-150, 0, 360, x=200, y=300, batch=self.screenhandler.main_batch))        
        if self.player_dude.abs_x>580 and not self.triggers[1]:
            self.triggers[1] = True
            self.screenhandler.game_objects.append(alien_enemy.AlienEnemy(1080,281,40,40,self.tile_data,-400, 360, 540, scripted_jump=[['left',900,500],['left',700,650]], x=200, y=300, batch=self.screenhandler.main_batch))
            self.screenhandler.game_objects.append(alien_enemy.AlienEnemy(950,281,40,40,self.tile_data,-300, 360, 540, scripted_jump=[['left',900,800],['left',700,850]], x=200, y=300, batch=self.screenhandler.main_batch))
        if self.player_dude.abs_x>980 and not self.triggers[2]:
            self.triggers[2] = True
            self.screenhandler.game_objects.append(alien_enemy.AlienEnemy(460,221,40,40,self.tile_data,400, scripted_jump=[['right',500,400],['right',720,800],['right',1080,800]], x=200, y=300, batch=self.screenhandler.main_batch))
            self.screenhandler.game_objects.append(alien_enemy.AlienEnemy(430,221,40,40,self.tile_data,450, scripted_jump=[['right',500,350],['right',720,750],['right',1080,800]], x=200, y=300, batch=self.screenhandler.main_batch))
            self.screenhandler.game_objects.append(alien_enemy.AlienEnemy(400,221,40,40,self.tile_data,550, scripted_jump=[['right',500,300],['right',720,700],['right',1080,800]], x=200, y=300, batch=self.screenhandler.main_batch))
        if self.player_dude.abs_x>2680 and not self.triggers[3]:
            self.triggers[3] = True
            self.screenhandler.go_to_screen(0,0)
                           

    def _handle_collisions(self):
        jumpkills = 0
        shotkills = 0
        for enemy in filter(lambda x: isinstance(x,alien_enemy.AlienEnemy), self.screenhandler.game_objects): #Loop through enemies
            if self.player_dude.handle_collision_with(enemy)=='kill':
                enemy.tagged_for_removal = True
                jumpkills += 1
            elif enemy.check_bounds():
                enemy.tagged_for_removal = True
            elif self._handle_bullet_collision(enemy):
                enemy.tagged_for_removal = True
                shotkills += 1
                self.graphic_effects.append(graphic_effects.KillEffect(enemy.abs_x,enemy.abs_y,60))     
        if jumpkills>0:
            resources.test_kill_sound.play()
            self.graphic_effects.append(graphic_effects.KillEffect(self.player_dude.abs_x,self.player_dude.abs_y,60))
            self.score += 2**jumpkills
            if self.score > self.screenhandler.hiscore:
                self.screenhandler.hiscore = self.score
            self.score_label.text = 'Score: '+str(self.score)+' Best: '+str(self.screenhandler.hiscore)
        if shotkills>0:
            resources.test_kill_sound.play()
            self.score += 2**jumpkills
            if self.score > self.screenhandler.hiscore:
                self.screenhandler.hiscore = self.score
            self.score_label.text = 'Score: '+str(self.score)+' Best: '+str(self.screenhandler.hiscore)            

    def _handle_bullet_collision(self, enemy):
        for shot in filter(lambda x: isinstance(x,bullet.Bullet), self.screenhandler.game_objects): #Loop through bullets
            if box_collide(shot.hitbox, enemy.hitbox):
                return True
        return False

    def notify(self, event):
        if event.name=='Go To Menu Request' and not self.going_to_menu:
            self.going_to_menu = True
            pyglet.clock.schedule_once(self.screenhandler.go_to_screen, 0.2, 0)
        elif event.name=='Print Debug Request':
            for obj in enumerate(self.screenhandler.game_objects):
                print(obj)
        if event.name=='Bullet Fired':
            self.screenhandler.game_objects.append(bullet.Bullet(event.x,event.y,event.width,event.height,event.tile_data,event.x_vel,event.y_vel, batch=self.screenhandler.main_batch))

    
    
        